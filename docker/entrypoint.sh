#!/bin/sh

set -e

if [ "${DEVEL_ENABLED}" = "1" ] && [ -n "${HOST_UID}" ] && [ -n "${HOST_GID}" ]; then
  NODE_UID="${HOST_UID}"
  NODE_GID="${HOST_GID}"
fi

if [ -n "${NODE_UID}" ] && [ -n "${NODE_GID}" ]; then
  # Delete node user
  deluser node || true
  # Delete the user occupying the uid we want to use.
  OLD_UID=$(getent passwd "$HOST_UID" | cut -d: -f1)
  if [ -n "${OLD_UID}" ]; then
    deluser  $OLD_UID || true
  fi
  addgroup -g ${NODE_GID} node
  adduser -D -h /home/node -s /bin/sh -u ${NODE_UID} -G node node

  chown -R node:node /home/node

  su node -c "$@"
else
  exec "$@"
fi

